﻿#include <iostream>
#include <cmath>
#include <chrono>


#define pi 3.1415926535
#define N 4 //количество полиномиальных коэффициентов

void genFirst(double* ind, int countParents) {

	for (int i = 0; i < countParents; i++) {
		for (int n = 0; n < N; n++) {// Заполнение массива
			if (std::rand() / static_cast<double>(RAND_MAX) > 0.5)
				ind[i * N + n] = std::rand() / static_cast<double>(RAND_MAX);
			else
				ind[i * N + n] = -std::rand() / static_cast<double>(RAND_MAX);
		}
	}
}

void fitness(int count, int countInd, double* childrens, double* arrInd) {

	double faterr = 0.0;
	double err;
	double h = 2.0 * pi / count;
	double X;
	
	for (int i = 0; i < countInd; i++) {

		for (int j = 0; j < count; j++) {
			err = 0.0;
			for (int k = 0; k < N; k++) {
				X = pow(j * h + h, k);
				err += childrens[i * N + k] * X;
			}
			faterr += pow(sin(j * h + h) - err, 2);
		}

		arrInd[i] = faterr;
		faterr = 0.0;
	}


}

void selectBestParents(int* indexes,double* arr, int countIndividuals, int countParents, double* bP, double* childrens) {
	
	double* arr_min = (double*)malloc(countParents * sizeof(double));

	for (int p = 0; p < countParents; p++) {
		arr_min[p] = DBL_MAX;
	}

	for (int i = 0; i < countIndividuals; i++) {
		if (arr[i] < arr_min[0]) {
			arr_min[0] = arr[i];
			indexes[0] = i;
		}
	}

	for (int p = 1; p < countParents; p++) {
		for (int i = 0; i < countIndividuals; i++) {
			if (arr[i] > arr_min[p-1] && arr[i] < arr_min[p]) {
				arr_min[p] = arr[i];
				indexes[p] = i;
			}
		}
	}


	for (int i = 0; i < countParents; i++) {
		for (int j = 0; j < N; j++) {
			bP[i * N + j] = childrens[indexes[i] * N + j];
		}

	}
		
}

int getRandomNumber(int min, int max)
{
	static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
	// Равномерно распределяем рандомное число в нашем диапазоне
	return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

void crossover(double* parent1, double* parent2,double* child) {

	int n = getRandomNumber(0,N);
	for (int i = 0; i < n; i++) {
		child[i] = parent1[i];
	}

	for (int i = n; i < N; i++) {
		child[i] = parent2[i];
	}

}

void mutation(double* child, double Em, double Dm) {
	
	int n = getRandomNumber(0, N);
	double d = Dm * std::rand() / 32767.0;
	double m = Em;
	if (std::rand() / 32767.0 > 0.5)
		m += d;
	else
		m -= d;

	if(std::rand() / 32767.0 > 0.5)
		child[n] += m;
	else
		child[n] -= m;
}

void makeChildren(double* parents, double* childrens, int countParents, int countInd, double Em, double Dm) {
	
	double* child;
	double* p1;
	double* p2;

	p1 = (double*)malloc(N * sizeof(double));
	p2 = (double*)malloc(N * sizeof(double));
	child = (double*)malloc(N * sizeof(double));

	for (int i = 0; i < countParents; i++) {
		int l=0;
		// берет последовательно по паре из набора родителей
		/*
		for (int k = 0; k < N; k++) {
			p1[k] = parents[i*2*N + k];
			p2[k] = parents[(i*2+1)*N + k];
			l++;
		}*/
		//берет самого лучшего и самого худшего по фенотипу 
		for (int k = 0; k < N; k++) {
			p1[k] = parents[i * N + k];
			p2[k] = parents[(countParents - i) * N + k];
			l++;
		}
		int temp = countInd / countParents;
		for (int j = 0; j < temp; j++) {

			crossover(p1, p2, child);

			if (std::rand() / 32767.0 > 0.5) {
				mutation(child, Em, Dm);
			}

			for (int k = 0; k < N; k++) {	
				childrens[i*temp + j*N + k] = child[k];
			}
		}
	}
}

int main()
{	
	int count, countInd, countParents, maxIter, maxConstIter;
	double Em, Dm;

	std::cout << "Enter count of points (500 - 1000): " << std::endl;
	std::cin >> count;

	std::cout << "Enter count of individuals (1000 - 2000): " << std::endl;
	std::cin >> countInd;

	std::cout << "Enter mean for Mutation: " << std::endl;
	std::cin >> Em;

	std::cout << "Enter varience for Mutation: " << std::endl;
	std::cin >> Dm;

	std::cout << "Enter count pair of parents: " << std::endl;
	std::cin >> countParents;

	countParents *= 2;

	std::cout << "Enter maximal count of epochs: " << std::endl;
	std::cin >> maxIter;

	std::cout << "Enter maximal count of epochs with same results: " << std::endl;
	std::cin >> maxConstIter;

	//////////////////////////////////////////////////////
	//////////Create first Lilith's childrens/////////////
	//////////////////////////////////////////////////////

	double* bP = (double*)malloc(countParents * N * sizeof(double));

	genFirst(bP, countParents);

	//////////////////////////////////////////////////////
	//////////Do the evolution like Pearl Jam/////////////
	//////////////////////////////////////////////////////

	int* indexes = (int*)malloc(countParents * sizeof(int));
	double* arrInd = (double*)malloc(countInd * sizeof(double));
	double* childrens = (double*)malloc(countInd * N * sizeof(double));

	double min = DBL_MAX, val = DBL_MAX;
	int indBest, sameIter = 1;
	int sumTime = 0;
	auto begin = std::chrono::steady_clock::now();
	auto end = std::chrono::steady_clock::now();
	auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
	for (int epoch = 1; epoch <= maxIter; epoch++) {

		begin = std::chrono::steady_clock::now();

		makeChildren(bP, childrens, countParents, countInd, Em, Dm);
		
		fitness(count, countInd, childrens, arrInd);

		selectBestParents(indexes, arrInd, countInd, countParents, bP, childrens);

		end = std::chrono::steady_clock::now();
		elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
		sumTime += elapsed_ms.count();
		//std::cout <<"Epoch " << epoch << ": " <<arrInd[indexes[0]]<< ' ' << arrInd[indexes[1]] << std::endl;
		std::cout << "Epoch " << epoch << ": " << arrInd[indexes[0]] << std::endl;
		if (arrInd[indexes[0]] < min) {
			min = arrInd[indexes[0]];
			indBest = epoch;
		}

		if (val == arrInd[indexes[0]]) {
			sameIter++;
		}

		else {
			val = arrInd[indexes[0]];
			sameIter = 1;
		}

		if (sameIter >= maxConstIter){
			std::cout << "Same " << maxConstIter << " iterations" << std::endl;
			break;
		}
	}


	std::cout << "time: " << sumTime << std::endl;
	std::cout << "min: " << min << std::endl << "epoch: " << indBest << std::endl;

	double* temp = (double*)malloc(N * sizeof(double));
	for (int j = 0; j < N; j++) {
		std::cout << bP[j] <<"*x^"<<j;
		if (j + 1 < N) {
			std::cout << "+ ";
		}
	}
	std::cout << std::endl;
	free(bP);

}


