
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <curand_kernel.h>

#include <thrust/sort.h>
#include <thrust/execution_policy.h>

#include <stdio.h>
#include <cmath>
#include <float.h>
#include <chrono>
#include <iostream>

#define BLOCK_SIZE 16
#define pi 3.1415926535
#define N 4 //количество полиномиальных коэффициентов

void fitness(int count, int countInd, double* childrens, double* arrInd) {

	double faterr = 0.0;
	double err;
	double h = pi / count;
	double X;

	for (int i = 0; i < countInd; i++) {

		for (int j = 0; j < count; j++) {
			err = 0.0;
			for (int k = 0; k < N; k++) {
				X = pow(j * h + h, k);
				err += childrens[i * N + k] * X;
			}
			faterr += pow(sin(j * h + h) - err, 2);
		}

		arrInd[i] = faterr;
		faterr = 0.0;
	}


}

__global__ void fitness_GPU(int count, int countInd, double* childrens, double* arrInd) {
	double faterr = 0.0;
	double err;
	double h = pi / count;
	double X;
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	if (index < countInd) {
		for (int i = index; i < countInd; i+=stride) {

			for (int j = 0; j < count; j++) {
				err = 0.0;
				for (int k = 0; k < N; k++) {
					X = pow(j * h + h, k);
					err += childrens[i * N + k] * X;
				}
				faterr += pow(sin(j * h + h) - err, 2);
			}

			arrInd[i] = faterr;
			faterr = 0.0;
		}
	}
}

void selectBestParents(int* h_indexes, double* h_arrInd, int countInd, int countParents, double* h_bP, double*h_childrens) {


	for (int i = 0; i < countInd; i++) {
		h_indexes[i] = i;
	}

	thrust::sort_by_key(h_arrInd, h_arrInd + countInd, h_indexes);

	for (int i = 0; i < countParents; i++) {
		for (int j = 0; j < N; j++) {
			h_bP[i * N + j] = h_childrens[N * h_indexes[i] + j];
		}
	}

}

 __global__ void Breeding_GPU(int Em, int Dm, int count, int countInd, double* d_bP, double* d_arrInd, double* d_childrens) {

	int countParents = 10;

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	if(index < countParents){
		for (int i = index; i < countParents; i += stride) {
			int countChildren = countInd / countParents;
			for (int j = 0; j < countChildren; j++) {

				curandState state;
				curand_init((unsigned long long)clock() + index, 0, 0, &state);

				int n = floor(curand_uniform_double(&state) * N);

				//берет самого лучшего и самого худшего по фенотипу 

				for (int k = 0; k < n; k++) {
					d_childrens[i * countChildren * N + j * N + k] = d_bP[i * N + k];
				}

				for (int k = n; k < N; k++) {
					d_childrens[i * countChildren * N + j * N + k] = d_bP[(countParents - i) * N + k];
				}
				curand_init((unsigned long long)clock() + index, 0, 0, &state);

				if (curand_uniform_double(&state) > 0.5) {

					double d = Dm * curand_uniform_double(&state);
					double m = Em;
					if (curand_uniform_double(&state) > 0.5)
						m += d;
					else
						m -= d;
					int nn = (int)(curand_uniform_double(&state) * N);
					if (curand_uniform_double(&state) > 0.5)
						d_childrens[i * countChildren * N + j * N + nn] += m;
					else
						d_childrens[i * countChildren * N + j * N + nn] -= m;
				}
			}
		}
	}	
	
}

int main()
{	
	
	int count, countInd, countParents, maxIter, maxConstIter;
	double Em, Dm;

	countParents = 10;

	std::cout << "Enter count of points (500 - 1000): " << std::endl;
	std::cin >> count;

	std::cout << "Enter count of individuals (1000 - 2000): " << std::endl;
	std::cin >> countInd;

	std::cout << "Enter mean for Mutation: " << std::endl;
	std::cin >> Em;

	std::cout << "Enter varience for Mutation: " << std::endl;
	std::cin >> Dm;

	std::cout << "Enter maximal count of epochs: " << std::endl;
	std::cin >> maxIter;

	std::cout << "Enter maximal count of epochs with same results: " << std::endl;
	std::cin >> maxConstIter;
	
	//////////////////////////////////////////////////////
	//////////Create first Lilith's childrens/////////////
	//////////////////////////////////////////////////////
	double* h_bP = new double[countParents * N];
	for (int i = 0; i < countParents; i++) {
		for (int j = 0; j < N; j++) {
			h_bP[i * N + j] = 0.0;
		}
	}

	//////////////////////////////////////////////////////
	//////////Do the evolution like Pearl Jam/////////////
	//////////////////////////////////////////////////////
	int* h_indexes = new int[countInd];
	double* h_arrInd = new double[countInd];
	double* h_childrens = new double[countInd * N];

	for (int i = 0; i < countInd; i++) {
		for (int j = 0; j < N; j++) {
			h_childrens[i * N + j] = 0.0;
		}
	}


	int* d_indexes;
	double* d_arrInd;
	double* d_bP;
	double* d_childrens;
	double min = DBL_MAX, val = DBL_MAX;
	int indBest, sameIter = 1;


	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	auto begin = std::chrono::steady_clock::now();
	auto end = std::chrono::steady_clock::now();
	auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
	float gpu_elapsed_time_ms;
	float sumTime = 0.0;
	
	
	cudaMalloc((void**)&d_arrInd, countInd * sizeof(double));
	cudaMalloc((void**)&d_bP, countParents * N * sizeof(double));
	cudaMalloc((void**)&d_childrens, countInd * N * sizeof(double));
	//cudaMalloc((void**)&d_indexes, countInd * sizeof(int));

	for (int epoch = 1; epoch <= maxIter; epoch++) {
		
		
		cudaEventRecord(start, 0);
		cudaMemcpy(d_bP, h_bP, countParents * N * sizeof(double), cudaMemcpyHostToDevice);
	
		Breeding_GPU<<<countParents * countInd, 1>>>(Em, Dm, count, countInd, d_bP, d_arrInd, d_childrens);

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&gpu_elapsed_time_ms, start, stop);

		sumTime += gpu_elapsed_time_ms;

		
		cudaMemcpy(h_childrens, d_childrens, countInd * N * sizeof(double), cudaMemcpyDeviceToHost);
		begin = std::chrono::steady_clock::now();

		fitness(count, countInd, h_childrens, h_arrInd);
		selectBestParents(h_indexes, h_arrInd, countInd, countParents, h_bP, h_childrens);

		end = std::chrono::steady_clock::now();

		elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);

		sumTime += elapsed_ms.count();

		std::cout << "Epoch " << epoch << ": " << h_arrInd[0] << std::endl;
		
		
		if (h_arrInd[0] < min) {
			min = h_arrInd[0];
			indBest = epoch;
		}

		if (val == h_arrInd[0]) sameIter++;

		else {
			val = h_arrInd[0];
			sameIter = 1;
		}

		if (sameIter >= maxConstIter) {
			std::cout << "Same " << maxConstIter << " iterations" << std::endl;
			break;
		}
		
	}
	
	
	std::cout << sumTime << std::endl;

	std::cout << "min: " << min << std::endl << "epoch: " << indBest << std::endl;

	double* temp = (double*)malloc(N * sizeof(double));
	for (int j = 0; j < N; j++) {
		std::cout << h_bP[j] << "*x^" << j;
		if (j + 1 < N) {
			std::cout << "+ ";
		}
	}
	std::cout << std::endl;
	
	cudaFree(d_arrInd);
	cudaFree(d_bP);
	cudaFree(d_childrens);
	//cudaFree(d_indexes);
	
}
